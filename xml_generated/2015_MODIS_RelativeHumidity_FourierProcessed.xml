<gmd:MD_Metadata xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gmi="http://standards.iso.org/iso/19115/-2/gmi/1.0" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:gmlcov="http://www.opengis.net/gmlcov/1.0" xmlns:gmlrgrid="http://www.opengis.net/gml/3.3/rgrid" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <gmd:fileIdentifier>
    <gco:CharacterString>c4702d92-80b3-11ee-b962-0242ac120014</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="eng" codeSpace="ISO 639-2">English</gmd:LanguageCode>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_CharacterSetCode" codeListValue="utf8">utf8</gmd:MD_CharacterSetCode>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MX_ScopeCode" codeListValue="dataset" codeSpace="ISOTC211/19115">dataset</gmd:MD_ScopeCode>
  </gmd:hierarchyLevel>
  <gmd:dateStamp>
    <gco:DateTime>2024-08-28T16:32:29</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>2015_MODIS_RelativeHumidity_FourierProcessed</gco:CharacterString>
          </gmd:title>
          <gmd:identifier>
            <gmd:MD_Identifier/>
          </gmd:identifier>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>This is a set of images produced by Temporal Fourier Analysis (TFA) of MODIS Relative Humidity data:
 
 Abstract :
 
 This is a set of images produced by temporal Fourier analysis of global MODIS data for Middle Infra Red (MID) derived from the MOD11A2 Product from USGS. The imagery summarises key environmental indicators, incorporating seasonal dynamics, for the MOOD study area.
 MODIS is a sensor on two NASA satellites, providing near-daily coverage of the entire Earth. The MOD11A2 product contains an 8-day average of MIR at 1-kilometre resolution. Reflectance values have been adjusted to remove the distortion caused by the view angle and land surface texture.
 
  
 
 The smoothed series was sampled at 5-day intervals and transformed into a set of sine curves describing annual, bi-annual, and tri-annual fluctuations. For each of these curves, the Fourier algorithm generated images expressing the amplitude, phase, and variance. Other outputs recorded the mean, minimum, and maximum of the time series, and error measured during the Fourier transform. For a detailed description of the Fourier algorithm and its output, please see the article by Scharlemann et al., 2008 (https://doi.org/10.1371/journal.pone.0001408)
 Sea pixels were masked with a VIIRS land/sea layer and the images were projected from sinusoidal to geographic. The MOOD study region was a subset of global images. Idrisi rasters were converted to GeoTIFF format to give data users more flexibility.
 
  
 
 File naming scheme: 
 
 The last two characters of each file name denote the output from Fourier processing:
 a0 - mean
 mn - minimum
 mx - maximum
 a1 - amplitude of annual cycle
 a2 - amplitude of bi-annual cycle
 a3 - amplitude of tri-annual cycle
 p1 - phase of annual cycle
 p2 - phase of bi-annual cycle
 p3 - phase of tri-annual cycle
 d1 - variance in annual cycle
 d2 - variance in bi-annual cycle
 d3 - variance in tri-annual cycle
 da - combined variance in annual, bi-annual, and tri-annual cycles
 vr - variance in raw data
 
  
 
 
 Projection + EPSG code:
 Latitude-Longitude/WGS84 (EPSG: 4326)
 Spatial extent:
 Extent -32.0000000000000000,10.0000000000000000 : 68.9999999999999574,81.9999999999999716
 Spatial resolution:
 0.0083333 deg (approx. 1000 m) 
 Temporal resolution:
  Decadal 
 
 Pixel values
 
 Parameter Fourier Variable Image values are
 RH values A0, A1, A2, A3, Min, Max, Vr Reflectance values 
 
 ALL D1,D2,D3,Da Percentages
 ALL E1,E2,E3 Percentages
 ALL P1,P2.P3 Months*100. (Jan=100)
 
 
 Source: 
 Middle Infra Red (MID) derived from the MOD11A2(MODIS NASA) Product from USGS
 
 
 Software used:
 Codes for modelling are in Python and C++
 The software used for map production is ESRI ArcMap 10.8</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>ERGO
</gco:CharacterString>
          </gmd:organisationName>
          <gmd:role>
            <gmd:CI_RoleCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode" codeListValue="principalInvestigator" codeSpace="ISOTC211/19115">principalInvestigator</gmd:CI_RoleCode>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:graphicOverview>
        <gmd:MD_BrowseGraphic>
          <gmd:fileName>
            <gco:CharacterString>https://gitlab.irstea.fr/umr-tetis/mood/geonetwork-insertion/-/raw/master/readme.img/ergo/Overview_82094a1.png</gco:CharacterString>
          </gmd:fileName>
          <gmd:fileDescription>
            <gco:CharacterString>thumbnail</gco:CharacterString>
          </gmd:fileDescription>
          <gmd:fileType>
            <gco:CharacterString>image/png</gco:CharacterString>
          </gmd:fileType>
        </gmd:MD_BrowseGraphic>
      </gmd:graphicOverview>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>MODIS</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>Yearly,RelativeHumidity</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>Fourier Processed</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>Covariate</gco:CharacterString>
          </gmd:keyword>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>CC-BY 4.0 Users are free to use, copy, distribute, transmit, and adapt the work for commercial and non-commercial purposes, without restriction, as long as clear attribution of the source is provided.</gco:CharacterString>
          </gmd:useLimitation>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:language>
        <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="eng" codeSpace="ISO 639-2">English</gmd:LanguageCode>
      </gmd:language>
      <gmd:characterSet>
        <gmd:MD_CharacterSetCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_CharacterSetCode" codeListValue="utf8">utf8</gmd:MD_CharacterSetCode>
      </gmd:characterSet>
      <gmd:topicCategory>
        <gmd:MD_TopicCategoryCode>health</gmd:MD_TopicCategoryCode>
      </gmd:topicCategory>
    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://doi.org/10.5281/zenodo.13122979</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString>Access to data</gco:CharacterString>
              </gmd:name>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL/>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString>Access to code</gco:CharacterString>
              </gmd:name>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL/>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString>Access to publication</gco:CharacterString>
              </gmd:name>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <!--Metadata Creation date/time: 2024-08-28T16:32:29-->
  <!--ISO 19139 XML generated by geometa R package - Version 0.6-2-->
  <!--ISO 19139 XML compliance: NO-->
  <!--geometa R package information:	Contact: Emmanuel Blondel emmanuel.blondel1@gmail.com	URL: https://github.com/eblondel/geometa/wiki	BugReports: https://github.com/eblondel/geometa/issues-->
</gmd:MD_Metadata> 